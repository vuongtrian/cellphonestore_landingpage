import React, { Component } from 'react';
import Product from '../Product/product'

class ProductList extends Component {
    render() {
        return (
            <div className="bg-dark text-white p-5">
                <h1 className='display-4 text-center'>BEST SMARTPHONE</h1>
                <div className='container-fluid'>
                    <Product/>
                </div>
            </div>
        );
    }
}

export default ProductList;