import React, { Component } from "react";

const data = [
  {
    id: "sp_1",
    name: "iphoneX",
    price: "30.000.000 VND",
    screen: "screen_1",
    backCamera: "backCamera_1",
    frontCamera: "frontCamera_1",
    img:
      "https://sudospaces.com/mobilecity-vn/images/2019/12/iphonex-black.jpg",
    desc:
      "iPhone X features a new all-screen design. Face ID, which makes your face your password",
  },
  {
    id: "sp_2",
    name: "Note 7",
    price: "20.000.000 VND",
    screen: "screen_2",
    backCamera: "backCamera_2",
    frontCamera: "frontCamera_2",
    img:
      "https://www.xtmobile.vn/vnt_upload/product/01_2018/thumbs/600_note_7_blue_600x600.png",
    desc:
      "The Galaxy Note7 comes with a perfectly symmetrical design for good reason",
  },
  {
    id: "sp_3",
    name: "Vivo",
    price: "10.000.000 VND",
    screen: "screen_3",
    backCamera: "backCamera_3",
    frontCamera: "frontCamera_3",
    img: "https://www.gizmochina.com/wp-content/uploads/2019/11/Vivo-Y19.jpg",
    desc:
      "A young global smartphone brand focusing on introducing perfect sound quality",
  },
  {
    id: "sp_4",
    name: "Blacberry",
    price: "15.000.000 VND",
    screen: "screen_4",
    backCamera: "backCamera_4",
    frontCamera: "frontCamera_4",
    img:
      "https://cdn.tgdd.vn/Products/Images/42/194834/blackberry-keyone-3gb-600x600.jpg",
    desc:
      "BlackBerry is a line of smartphones, tablets, and services originally designed",
  },
];

class Product extends Component {
  renderProduct() {
    return data.map((item) => {
      return (
        <div className="col-3">
          <div className="card">
            <img src={item.img} alt="cellphone" height="300px" />
            <div className="card-body text-body p-2">
              <h3 className="m-0">{item.name}</h3>
              <p className="m-0">{item.desc}</p>
            </div>
            <div className="card-button p-2 ">
              <button
                type="button"
                className="btn btn-primary m-1"
                data-toggle="modal"
                data-target="#myModal"
              >
                Detail
              </button>
              <button type="button" className="btn btn-danger m-1">
                Cart
              </button>
            </div>

            {/* The Modal */}
            <div className="modal fade" id="myModal">
              <div className="modal-dialog">
                <div className="modal-content">
                  {/* Modal Header */}
                  <div className="modal-header text-body">
                    <h4 class="modal-title">{item.name}</h4>
                    <button
                      type="button"
                      className="close"
                      data-dismiss="modal"
                    >
                      ×
                    </button>
                  </div>
                  {/* Modal body */}
                  <div className="modal-body text-body">
                    <img src={item.img} alt="cellphone" height="300px" />
                    <div>
                      <p>Screen: {item.screen}</p>
                      <p>Back Camera: {item.backCamera}</p>
                      <p>Front Camera: {item.frontCamera}</p>
                      <p>Price: {item.price}</p>
                    </div>
                  </div>
                  {/* Modal footer */}
                  <div className="modal-footer">
                    <button
                      type="button"
                      className="btn btn-danger"
                      data-dismiss="modal"
                    >
                      Close
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      );
    });
  }

  render() {
    return <div className="row">{this.renderProduct()}</div>;
  }
}

export default Product;
