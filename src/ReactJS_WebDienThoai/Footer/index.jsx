import React, { Component } from "react";
import pro1 from "../../img/promotion_1.png";
import pro2 from "../../img/promotion_2.png";
import pro3 from "../../img/promotion_3.jpg";
import './style.css';

class Footer extends Component {
  render() {
    return (
      <div className="container text-white">
        <h1>PROMOTION</h1>
        <div className="container bg-white p-2">
          <div className='w-100 d-flex justify-content-between'>
            <img className='pro__image-item' src={pro1} alt="Promotion Image 1" />
            <img className='pro__image-item' src={pro2} alt="Promotion Image 2" />
            <img className='pro__image-item' src={pro3} alt="Promotion Image 3" />
          </div>
        </div>
      </div>
    );
  }
}

export default Footer;
